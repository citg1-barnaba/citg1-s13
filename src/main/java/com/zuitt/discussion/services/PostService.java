package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    // Create a post
    void createPost(String stringToken, Post post);

    // Viewing all posts
    Iterable<Post> getPost();

    // Delete a post
    ResponseEntity deletePost(Long id, String stringToken);

    // Update a post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // To get all post of a specific user
    Iterable<Post> getMyPost(String stringToken);
}
