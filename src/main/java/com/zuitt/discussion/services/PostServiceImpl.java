// cointains the business logic concerned with a particular object in the class
package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
// Will allow us to use the CRUD repository method inherited from the CRUDRepository
@Service
public class PostServiceImpl implements PostService {
    // An object cannot be instantiated from interfaces.
    // @Autowired allow us to use the interface as if it was an instance of an object and allows us to use the methods
    // from the CrudRepository

    @Autowired
    private PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // Create Post
    public void createPost(String stringToken, Post post){
        // Retrieve the "User" object using the extracted username from the JWT Token\
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        postRepository.save(newPost);
    }

    // Get All Posts
    public Iterable<Post> getPost(){
        return postRepository.findAll();
    }

    // Delete post
    public ResponseEntity deletePost(Long id, String stringToken){
        /*postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully!", HttpStatus.OK);*/

        Post postForDeleting = postRepository.findById(id).get();
        String postAuthorName = postForDeleting.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUsername.equals(postAuthorName)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Saving and Update a post
    public ResponseEntity updatePost(Long id, String stringToken, Post post){
        /*// Find the post to update
        Post postForUpdate = postRepository.findById(id).get();

        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully!", HttpStatus.OK);*/

        Post postForUpdating = postRepository.findById(id).get();

        // Get the author of the specific post
        String postAuthorName = postForUpdating.getUser().getUsername();

        // Get the username from the stringToken to compare it with the username of the current post being edited
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        // Check if the username of the authenticated user matches the username of the post author
        if (authenticatedUsername.equals(postAuthorName)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());

            postRepository.save(postForUpdating);

            return new ResponseEntity("Post Updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }

    // To get all post of a specific user
    public Iterable<Post> getMyPost(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getPosts();
    }
}
