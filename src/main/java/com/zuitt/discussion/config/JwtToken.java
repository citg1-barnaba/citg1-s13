package com.zuitt.discussion.config;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken implements Serializable {

    // to get the token from application.properties
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    private static final long serialVersionUID = 7617765790436542941L;

    // Time duration in seconds that the token can be used.
    public static final long JWT_TOKEN_VALIDITY = 5*60*60; //5 hours

    // This generates the JWT  by setting the claims, subject, issuedAt, expiration, and signing
    private String doGenerateToken(Map<String, Object> claims, String subject){
        // Jwts.builder() creates a new JWT builder instance. This object is used to build and sign the JWT.
        return Jwts.builder()
                // .setClaims includes the information to show the recipient which is the username
                .setClaims(claims)
                // .setSubject adds information about the subject. (The subject username.)
                .setSubject(subject)
                // .setIssuedAt sets the time and date when the token was created
                .setIssuedAt(new Date(System.currentTimeMillis()))
                // .setExpiration sets the expiration of the token
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                // .signWith creates the token using a declared algorithm, with the secret keyword.
                // "HS512" is a secure cryptographic algorithm.
                // The "secret key" is passed as a parameter and is used to verify the signature later on.
                // .compact() is used to generate the final JWT string by compacting the JWT builder object.
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public String generateToken(UserDetails userDetails){
        Map<String, Object> claims = new HashMap<>();

        // Retrieves the user information from the userRepository by using the username from the UserDetails object
        User user = userRepository.findByUsername(userDetails.getUsername());

        // ID is added to the claims of JWT to be used to identify the user in the server.
        claims.put("user", user.getId());

        return doGenerateToken(claims, userDetails.getUsername());
    }

    // Token validation by extracting the username from the token
    public Boolean validateToken(String token, UserDetails userDetails){
        // Extract the username from the token and store it in a variable
        final String username = getUsernameFromToken(token);

        // Returns a Boolean value that indicates whether the token is valid or not.
        // First condition checks if the username in the userDetails matches thee username extracted from the token
        // Second condition checks if the token is expired by invoking the "isTokenExpired()" method
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    //This method extracts a specific claim from a JWT token by using functional interface.
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver){
        final Claims claims = getAllClaimsFromToken(token);
        // This extract the specific claim value.
        return claimsResolver.apply(claims);
    }

    // This extracts all the claims from the token.
    private Claims getAllClaimsFromToken(String token){
        // is used to parse a JWT token
        return Jwts.parser()
                //sets the secret key used to sign token
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    // To extract the subject(username) from JWT Token
    public String getUsernameFromToken(String token){
        String claim = getClaimFromToken(token, Claims::getSubject);
        return claim;
    }

    public Date getExpirationDateFromToken(String token){
        return getClaimFromToken(token, Claims::getExpiration);
    }

    // This is used to check if the JWT Token has expired
    private Boolean isTokenExpired(String token){
        final Date expiration = getExpirationDateFromToken(token);

        // This returns a boolean value that represents whether the current time is before the expiration date
        // of the JWT Token (expiration)
        // True, if the current time is after the expiration
        // False, if the current time is before the expiration
        return expiration. before(new Date());
    }
}
